;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; refresh' after modifying this file!

;;(setq user-full-name "Rhys Day"
;;      user-mail-address "rhysday@mailbox.org")

(global-auto-revert-mode t)

(add-hook 'org-mode-hook #'auto-fill-mode)

(setq doom-font (font-spec :family "monospace" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. These are the defaults.
(setq doom-theme 'doom-one)

(setq package-user-dir (expand-file-name "packages" user-emacs-directory))

;; Org files set to OVERVIEW when first opened. Change 't' to 'nil' if I want to showeverything
(setq org-startup-folded t)


;; Setting the neotree width to be adjustable.
(setq neo-window-fixed-size nil)

;;Transparency
(set-frame-parameter (selected-frame) 'alpha '(94 94))
;;(set-frame-parameter (selected-frame) 'alpha '(94 94))

;;Set default window size:
(defun set-frame-size-according-to-resolution ()
  (interactive)
  (if window-system
  (progn
    ;; use 120 char wide window for largeish displays
    ;; and smaller 80 column windows for smaller displays
    ;; pick whatever numbers make sense for you
    (if (> (x-display-pixel-width) 1280)
           (add-to-list 'default-frame-alist (cons 'width 120))
           (add-to-list 'default-frame-alist (cons 'width 80)))
    ;; for the height, subtract a couple hundred pixels
    ;; from the screen height (for panels, menubars and
    ;; whatnot), then divide by the height of a char to
    ;; get the height we want
    (add-to-list 'default-frame-alist
         (cons 'height (/ (- (x-display-pixel-height) 200)
                             (frame-char-height)))))))

 (set-frame-size-according-to-resolution)

;; If you want to change the style of line numbers, change this to `relative' or
;; `nil' to disable it:
(setq display-line-numbers-type t)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', where Emacs
;;   looks when you load packages with `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.
;;
;;Startup Messages
(setq inhibit-startup-message t
      initial-scratch-message ""
      inhibit-startup-echo-area-message t)

(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; To copy text from Emacs to system clipboard
(setq x-select-enable-clipboard t)

;; Single action undo
(setq evil-want-fine-undo 'fine)


(require 'epa-file)
(epa-file-enable)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;BROWSER

 (setq browse-url-browser-function 'browse-url-generic
       browse-url-generic-program "xdg-open")




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ELFEED
(global-set-key (kbd "C-x w") 'elfeed)

(use-package elfeed
  :ensure t
  :config
;;functions to support syncing .elfeed between machines
  (setq elfeed-db-directory "~/.config/doom/elfeeddb")

(setq elfeed-search-title-min-width 60)
(setq elfeed-search-title-max-width 160)
(setq elfeed-search-trailing-min-width 40))


;; Load elfeed-org
(use-package elfeed-org
  :ensure t
  :config
(elfeed-org)
(setq rmh-elfeed-org-files (list "~/org/elfeed.org")))


;;OPEN VIDEOS IN MPV
(require 'elfeed)

(defun elfeed-v-mpv (url)
  "Watch a video from URL in MPV"
  (async-shell-command (format "mpv '%s'" url)))

(defun elfeed-view-mpv (&optional use-generic-p)
  "Youtube-feed link"
  (interactive "P")
  (let ((entries (elfeed-search-selected)))
    (cl-loop for entry in entries
	     do (elfeed-untag entry 'unread)
	     when (elfeed-entry-link entry)
	     do (elfeed-v-mpv it))
    (mapc #'elfeed-search-update-entry entries)
    (unless (use-region-p) (forward-line))))

(define-key elfeed-search-mode-map (kbd "C-c y") 'elfeed-view-mpv)


;;DOWNLOAD VIDEOS WITH YOUTUBE-DL
(defun yt-dl-it (url)
  "Downloads the URL in an async shell"
  (let ((default-directory "~/Downloads/YouTube"))
    (async-shell-command (format "youtube-dl --ignore-config -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' --restrict-filenames '%s'" url))))

(defun elfeed-youtube-dl (&optional use-generic-p)
  "Youtube-DL link"
  (interactive "P")
  (let ((entries (elfeed-search-selected)))
    (cl-loop for entry in entries
             do (elfeed-untag entry 'unread)
             when (elfeed-entry-link entry)
             do (yt-dl-it it))
    (mapc #'elfeed-search-update-entry entries)
    (unless (use-region-p) (forward-line))))

(define-key elfeed-search-mode-map (kbd "C-c d") 'elfeed-youtube-dl)




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;DIRED

;;OPEN FILES IN EXTERNAL APP
(add-hook 'dired-mode-hook 'hl-line-mode)

(defun dired-open-file ()
  "In dired, open the file named on this line."
  (interactive)
  (let* ((file (dired-get-filename nil t)))
    (message "Opening %s" file)
    (call-process "xdg-open" nil 0 nil file)
    (message "Opened %s" file)))
(eval-after-load "dired"
    '(define-key dired-mode-map (kbd "C-c o") 'dired-open-file))

;; launches alacritty with "terminal-here" package
(setq terminal-here-terminal-command (list "alacritty"))

(define-key dired-mode-map (kbd "C-c t") 'terminal-here-launch)

(global-set-key (kbd "C-x 1")
  (lambda ()
    (interactive)
    (dired "~/")))

(global-set-key (kbd "C-x 2")
  (lambda ()
    (interactive)
    (dired "/mnt/Projects/")))

(global-set-key (kbd "C-x 3")
  (lambda ()
    (interactive)
    (dired "/mnt/10TB/")))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;DIRED RAINBOW

(use-package dired-rainbow
  :config
  (progn
    (dired-rainbow-define-chmod directory "#6cb2eb" "d.*")
    (dired-rainbow-define html "#eb5286" ("css" "less" "sass" "scss" "htm" "html" "jhtm" "mht" "eml" "mustache" "xhtml"))
    (dired-rainbow-define xml "#f2d024" ("xml" "xsd" "xsl" "xslt" "wsdl" "bib" "json" "msg" "pgn" "rss" "yaml" "yml" "rdata"))
    (dired-rainbow-define document "#9561e2" ("docm" "doc" "docx" "odb" "odt" "pdb" "pdf" "ps" "rtf" "djvu" "epub" "odp" "ppt" "pptx"))
    (dired-rainbow-define markdown "#ffed4a" ("org" "etx" "info" "markdown" "md" "mkd" "nfo" "pod" "rst" "tex" "textfile" "txt"))
    (dired-rainbow-define database "#6574cd" ("xlsx" "xls" "csv" "accdb" "db" "mdb" "sqlite" "nc"))
    (dired-rainbow-define media "#de751f" ("mp3" "mp4" "MP3" "MP4" "avi" "mpeg" "mpg" "flv" "ogg" "mov" "mid" "midi" "wav" "aiff" "flac"))
    (dired-rainbow-define image "#f66d9b" ("tiff" "tif" "cdr" "gif" "ico" "jpeg" "jpg" "png" "psd" "eps" "svg" "JPG"))
    (dired-rainbow-define log "#c17d11" ("log"))
    (dired-rainbow-define shell "#f6993f" ("awk" "bash" "bat" "sed" "sh" "zsh" "vim"))
    (dired-rainbow-define interpreted "#38c172" ("py" "ipynb" "rb" "pl" "t" "msql" "mysql" "pgsql" "sql" "r" "clj" "cljs" "scala" "js"))
    (dired-rainbow-define compiled "#4dc0b5" ("asm" "cl" "lisp" "el" "c" "h" "c++" "h++" "hpp" "hxx" "m" "cc" "cs" "cp" "cpp" "go" "f" "for" "ftn" "f90" "f95" "f03" "f08" "s" "rs" "hi" "hs" "pyc" ".java"))
    (dired-rainbow-define executable "#8cc4ff" ("exe" "msi"))
    (dired-rainbow-define compressed "#51d88a" ("7z" "zip" "bz2" "tgz" "txz" "gz" "xz" "z" "Z" "jar" "war" "ear" "rar" "sar" "xpi" "apk" "xz" "tar"))
    (dired-rainbow-define packaged "#faad63" ("deb" "rpm" "apk" "jad" "jar" "cab" "pak" "pk3" "vdf" "vpk" "bsp"))
    (dired-rainbow-define encrypted "#ffed4a" ("gpg" "pgp" "asc" "bfe" "enc" "signature" "sig" "p12" "pem"))
    (dired-rainbow-define fonts "#6cb2eb" ("afm" "fon" "fnt" "pfb" "pfm" "ttf" "otf"))
    (dired-rainbow-define partition "#e3342f" ("dmg" "iso" "bin" "nrg" "qcow" "toast" "vcd" "vmdk" "bak"))
    (dired-rainbow-define vc "#0074d9" ("git" "gitignore" "gitattributes" "gitmodules"))
    (dired-rainbow-define-chmod executable-unix "#38c172" "-.*x.*")
    ))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;ORG

;;daily review template
(defun my-new-daily-review ()
  (interactive)
  (let ((org-capture-templates '(("d" "Review: Daily Review" entry (file+olp+datetree "/tmp/reviews.org")
                                  (file "~/org/templates/dailyreviewtemplate.org")))))
    (progn
      (org-capture nil "d")
      (org-capture-finalize t)
      (org-speed-move-safe 'outline-up-heading)
      (org-narrow-to-subtree)
      (fetch-calendar)
      (org-clock-in))))

(bind-keys :prefix-map review-map
           :prefix "C-c r"
           ("d" . my-new-daily-review))

;; Capture template
(after! org

(require 'org-agenda)
;; Full screen org-agenda.
(setq org-agenda-window-setup 'only-window)

;;;Integrate mu4e with org-mode
;;(require 'org-mu4e)
;;;;store link to message if in header view, not to header query
;;(setq org-mu4e-link-query-in-headers-mode nil)

(setq org-capture-templates '(("t" "Todo [inbox]" entry
                              (file+headline "~/org/gtd.gpg" "Inbox")
                              "* TODO [#A] %i%?\n")
                              ("l" "Link [inbox]" entry
                              (file+headline "~/org/gtd.gpg" "Inbox")
                              "* TODO [#A] %i%?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n%a\n")
                              ("T" "Tickler" entry
                               (file+headline "~/org/gtd.gpg" "Tickler")
                               "* %i%? \n %U")
                              ("p" "Project" entry
                               (file+headline "~/org/gtd.gpg" "Projects")
                               "* PROJECT %i%? [%]")
                              ("s" "Someday" entry
                               (file+headline "~/org/gtd.gpg" "Someday")
                               "* SOMEDAY %?\n")
                              ("g" "Goal" entry
                               (file+headline "~/org/gtd.gpg" "Goals")
                               "* %?\n")
                              ))

(setq org-refile-targets '(("~/org/notes.gpg" :maxlevel . 1)
                           ("~/org/gtd.gpg" :maxlevel . 2)
                          ))

(setq org-todo-keywords '((sequence "TODO(t)" "WAITING(w)" "PROJECT(p)" "NEXT(n)" "SOMEDAY(s)" "GOAL(g)" "|" "DONE(d)" "CANCELLED(c)")))
;;
;;Colour of todo keywords
(setq org-todo-keyword-faces '(("PROJECT" . "#ECBE7B") ("WAITING" . "#c678dd") ("TODO" . "#51afef") ("NEXT" . "#c678dd") ("GOAL" . "#99bb66") ("SOMEDAY" . "#a9a1e1")))
;;
;;Colour of tags
(setq org-tag-faces '(("mimirs" . "#ECBE7B") ("jobsearch" . "#dd8844") ("bills" . "#ff6655") ("@appointments" . "#ff6655")))

(setq org-tag-alist '(
                      ;; Depth
                      ("process" . ?p)
                      ("immersive" . ?i)
                      ;; Context
                      ("@home" . ?h)
                      ("@work" . ?w)
                      ("@computer" . ?c)
                      ("@mobile" . ?m)
                      ("@errands" . ?e)
                      ("@appointments" . ?a)
                      ("@gym" . ?g)
                      ;; Type
                      ("inbox" . ?I)
                      ("daily" . ?d)
                      ("mimirs" . ?M)
                      ("bills" . ?B)
                      ("linux" . ?l)
                      ("emacs" . ?E)
                      ("research" . ?R)
                      ("tutorials" . ?T)
                      ("jobsearch" . ?j)
                      ;; Time
                      ("15min" . ?<)
                      ("30min" . ?=)
                      ("1h" . ?>)
                      ("3h" . ?/)
                      ;; Energy
                      ("easy" . ?1)
                      ("average" . ?2)
                      ("challenge" . ?3)
                      ))

;(set-pretty-symbols!
;  'org-mode :alist
;                     '(("TODO" . ?▲)
;                      ("WAITING" . ?⏳)
;                      ("PROJECT" . ?✠)
;                      ("NEXT" . ?➤)
;                      ("DONE" . ?✓)
;                      ("CANCELLED" . ?✘)
;                      ))

(setq org-agenda-files (quote ("~/org/gtd.gpg"
                               "~/org/notes.gpg")))

(use-package! org-ql
  :after org
  :commands org-ql-search
  :config
)

(use-package! org-ql-agenda
  :after org
  :commands org-ql-agenda
  )

(use-package! org-sidebar
  :commands (org-sidebar org-sidebar-tree org-sidebar-ql)
  )

(use-package! org-super-agenda
  :after org-agenda
  :init
  (setq
        org-agenda-skip-scheduled-if-done t
        org-agenda-skip-deadline-if-done t
        org-agenda-include-deadlines t
        org-agenda-show-current-time-in-grid nil
        org-agenda-block-separator nil
        org-agenda-compact-blocks t
        org-agenda-start-day nil ;; i.e. today
        org-agenda-span 1
        org-agenda-start-on-weekday t)
 (setq org-agenda-custom-commands
        '(("z" "Super view"
         ((agenda "" ((org-agenda-overriding-header "")
                        (org-super-agenda-groups
                         '((:name "Schedule"
                                  :time-grid t
                                   :transformer (--> it
                                  ;(upcase it)
                                  (propertize it 'face '(:foreground "#ECBE7B")))
                                  :order 1)
                         (:name "Today"
                                  :scheduled today
                                  :order 2)
                         (:name "Due Today"
                                 :deadline today
                                 :order 3)
                          (:name "Due Soon"
                                 :deadline future
                                 :order 4)
                          (:discard (:anything t))
                           ))))
          (alltodo "" ((org-agenda-overriding-header "")
                       (org-super-agenda-groups
                          '((:name "Reschedule"
                                 :scheduled past
                                 :order 1)
                          (:name "Scheduled Soon"
                                  :scheduled future
                                  :order 2)
                          (:name "Overdue"
                                 :deadline past
                                 :face (:background "black" :underline t)
                                 :order 3)
                          (:name "Inbox"
                           :tag "inbox"
                           :order 4)
                           (:name "Mimir's"
                                 :tag "Mimirs"
                                 :order 5)
                          (:name "Tutorials"
                                 :tag "tutorials"
                                 :order 6)
                          (:name "Linux"
                                 :tag "linux"
                                 :tag "emacs"
                                 :order 7)
                          (:name "Waiting"
                                 :todo "WAITING"
                                 :order 11)
                          (:name "Projects"
                                 :todo "PROJECT"
                                 :order 15)
                          (:name "Someday"
                                 :todo "SOMEDAY"
                                 :order 16)
                          (:discard (:anything t))
                          ))))))))
 :config
  (org-super-agenda-mode)
))

(defun auto-save-command ()
  (let* ((basic (and buffer-file-name
                     (buffer-modified-p (current-buffer))
                     (file-writable-p buffer-file-name)
                     (not org-src-mode)))
         (proj (and (projectile-project-p)
                    basic)))
    (if proj
        (projectile-save-project-buffers)
      (when basic
        (save-buffer)))))

(defmacro advise-commands (advice-name commands class &rest body)
  "Apply advice named ADVICE-NAME to multiple COMMANDS.
The body of the advice is in BODY."
  `(progn
     ,@(mapcar (lambda (command)
                 `(defadvice ,command (,class ,(intern (concat (symbol-name command) "-" advice-name)) activate)
                    ,@body))
               commands)))

(advise-commands "auto-save"
                 (ido-switch-buffer ace-window magit-status windmove-up windmove-down windmove-left windmove-right mode-line-other-buffer)
                 before
                 (auto-save-command))

(add-hook 'mouse-leave-buffer-hook 'auto-save-command)
(add-hook 'focus-out-hook 'auto-save-command)

(bind-key "C-x C-s" 'save-buffer)

(defvar backup-dir (expand-file-name "~/.config/doom/emacs_backup/"))
(defvar autosave-dir (expand-file-name "~/.config/doom/autosave/"))
(setq backup-directory-alist (list (cons ".*" backup-dir))
      auto-save-list-file-prefix autosave-dir
      auto-save-file-name-transforms `((".*" ,autosave-dir t))
      tramp-backup-directory-alist backup-directory-alist
      tramp-auto-save-directory autosave-dir)


;;;; Hide everything from :PROPERTIES: through :END:
(defun org-cycle-hide-drawers (state)
  "Re-hide all drawers after a visibility state change."
  (when (and (derived-mode-p 'org-mode)
             (not (memq state '(overview folded contents))))
    (save-excursion
      (let* ((globalp (memq state '(contents all)))
             (beg (if globalp
                    (point-min)
                    (point)))
             (end (if globalp
                    (point-max)
                    (if (eq state 'children)
                      (save-excursion
                        (outline-next-heading)
                        (point))
                      (org-end-of-subtree t)))))
        (goto-char beg)
        (while (re-search-forward org-drawer-regexp end t)
          (save-excursion
            (beginning-of-line 1)
            (when (looking-at org-drawer-regexp)
              (let* ((start (1- (match-beginning 0)))
                     (limit
                       (save-excursion
                         (outline-next-heading)
                           (point)))
                     (msg (format
                            (concat
                              "org-cycle-hide-drawers:  "
                              "`:END:`"
                              " line missing at position %s")
                            (1+ start))))
                (if (re-search-forward "^[ \t]*:END:" limit t)
                  (outline-flag-region start (point-at-eol) t)
                  (user-error msg))))))))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;EMAIL
                                        ;(eval-when-compile
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu4e/")
(require 'mu4e)
(require 'smtpmail)
(setq mail-user-agent 'mu4e-user-agent)

;; the maildirs you use frequently; access them with 'j' ('jump')
(setq mu4e-maildir-shortcuts
      '( ("/INBOX"    . ?i)
         ("/Archive"  . ?a)
         ("/Sent"     . ?s)
         ("/Trash"    . ?t)
         ("/Drafts"   . ?d)))

(setq ;; signature
      mu4e-compose-signature (concat
                           "Rhys Day\n"
                           "Email: rhysday@mailbox.org\n"
                           "\n"))

(setq  ;; account info
      user-mail-address "rhysday@mailbox.org"
      user-full-name  "Rhys Day"
     ;; mu4e-compose-reply-to-address "email@rhysday.com"
      message-send-mail-function 'smtpmail-send-it
      smtpmail-default-smtp-server "smtp.mailbox.org"
      smtpmail-smtp-server "smtp.mailbox.org"
      smtpmail-smtp-user "rhysday@mailbox.org"
      smtpmail-stream-type 'tls
      smtpmail-smtp-service 465

      mu4e-maildir "~/.local/share/mail/mailbox"
      mu4e-sent-folder   "/Sent"
      mu4e-drafts-folder "/Drafts"
      mu4e-trash-folder  "/Trash"
      mu4e-refile-folder "/Archive"
      )

(setq
      mu4e-get-mail-command "mbsync -c /home/me/.config/isync/mbsyncrc mailbox"
    ;;  mu4e-change-filenames-when-moving t
    ;;  mu4e-headers-auto-update t
      mu4e-html2text-command "w3m -T text/html"
      mu4e-update-interval 120
      mu4e-attachment-dir "~/Downloads"
      )

;;set up queue for offline email
;;use mu mkdir  ~/Maildir/queue to set up first
(setq smtpmail-queue-mail nil  ;; start in normal mode
      smtpmail-queue-dir   "~/.local/share/mail/mailbox/queue/cur")

;(setq message-signature-file "~/.emacs.d/.signature") ; put your signature in this file
;
;; don't keep message buffers around
(setq message-kill-buffer-on-exit t)

;; attempt to show images when viewing messages
(setq mu4e-view-show-images t
      mu4e-show-images t
      mu4e-view-image-max-width 800)

;; use imagemagick, if available
(when (fboundp 'imagemagick-register-types))

;; (setq mu4e-html2text-command "html2text -utf8 -width 72") ;; nil "Shel command that converts HTML
;; ref: http://emacs.stackexchange.com/questions/3051/how-can-i-use-eww-as-a-renderer-for-mu4e
(defun my-render-html-message ()
  (let ((dom (libxml-parse-html-region (point-min) (point-max))))
    (erase-buffer)
    (shr-insert-document dom)
    (goto-char (point-min))))
(setq mu4e-html2text-command 'my-render-html-message)

;; yt
(setq mu4e-view-prefer-html t) ;; try to render
(add-to-list 'mu4e-view-actions
             '("ViewInBrowser" . mu4e-action-view-in-browser) t) ;; read in browser
;; mu4e as default email agent in emacs
(setq mail-user-agent 'mu4e-user-agent)
(require 'org-mu4e)
                                        ;== M-x org-mu4e-compose-org-mode==
(setq org-mu4e-convert-to-html t) ;; org -> html
                                        ; = M-m C-c.=

;; give me ISO(ish) format date-time stamps in the header list
(setq  mu4e-headers-date-format "%Y-%m-%d %H:%M")

;; customize the reply-quote-string
;; M-x find-function RET message-citation-line-format for docs
(setq message-citation-line-format "%N @ %Y-%m-%d %H:%M %Z:\n")
(setq message-citation-line-function 'message-insert-formatted-citation-line)

;; the headers to show in the headers list -- a pair of a field
;; and its width, with `nil' meaning 'unlimited'
;; (better only use that for the last field.
;; These are the defaults:
;;(setq mu4e-headers-fields
;;    '( (:date          .  25)
;;       (:flags         .   6)
;;      (:from          .  22)
;;       (:subject       .  nil)))

(setq mu4e-use-fancy-chars 't)

;; <tab> to navigate to links, <RET> to open them in browser
(add-hook 'mu4e-view-mode-hook
  (lambda()
    ;; try to emulate some of the eww key-bindings
    (local-set-key (kbd "<RET>") 'mu4e~view-browse-url-from-binding)
    (local-set-key (kbd "<tab>") 'shr-next-link)
    (local-set-key (kbd "<backtab>") 'shr-previous-link)))

(use-package mu4e-alert
    :after mu4e
    :hook ((after-init . mu4e-alert-enable-mode-line-display)
           (after-init . mu4e-alert-enable-notifications))
    :config (mu4e-alert-set-default-style 'libnotify))
