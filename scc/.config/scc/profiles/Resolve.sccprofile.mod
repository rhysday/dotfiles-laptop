{
    "_": "", 
    "buttons": {
        "A": {
            "action": "button(Keys.KEY_ENTER)"
        }, 
        "B": {
            "action": "button(Keys.KEY_BACKSPACE)"
        }, 
        "BACK": {
            "action": "button(Keys.KEY_HOME)"
        }, 
        "C": {
            "action": "hold(menu('Default.menu'), menu('Default.menu'))"
        }, 
        "CPADPRESS": {
            "action": "button(Keys.BTN_LEFT)"
        }, 
        "LB": {
            "action": "button(Keys.BTN_RIGHT)"
        }, 
        "LGRIP": {
            "action": "button(Keys.KEY_LEFT)"
        }, 
        "LPAD": {
            "action": "button(Keys.KEY_LEFTCTRL) and button(Keys.KEY_B)"
        }, 
        "RB": {
            "action": "button(Keys.KEY_I)"
        }, 
        "RGRIP": {
            "action": "button(Keys.KEY_RIGHT)"
        }, 
        "RPAD": {
            "action": "button(Keys.KEY_LEFTCTRL) and button(Keys.KEY_Z)"
        }, 
        "START": {
            "action": "button(Keys.KEY_END)"
        }, 
        "STICKPRESS": {
            "action": "button(Keys.BTN_MIDDLE)"
        }, 
        "X": {
            "action": "button(Keys.KEY_LEFTCTRL) and button(Keys.KEY_F)"
        }, 
        "Y": {
            "action": "button(Keys.KEY_LEFTSHIFT) and button(Keys.KEY_Z)"
        }
    }, 
    "cpad": {
        "action": "mouse()"
    }, 
    "gyro": {}, 
    "is_template": false, 
    "menus": {}, 
    "pad_left": {
        "action": "ring(0.8, ball(XY(mouse(Rels.REL_HWHEEL), mouse(Rels.REL_WHEEL))), circular(axis(Axes.ABS_X) and button(Keys.KEY_LEFT, Keys.KEY_RIGHT)))"
    }, 
    "pad_right": {
        "action": "ring(0.89, mode(A, None, smooth(8, 0.78, 2.0, sens(2.0, 2.0, ball(mouse())))))"
    }, 
    "stick": {
        "action": "dpad8(button(Keys.KEY_K), button(Keys.KEY_K), button(Keys.KEY_J), button(Keys.KEY_L), button(Keys.KEY_J), button(Keys.KEY_L), button(Keys.KEY_J), button(Keys.KEY_L))"
    }, 
    "trigger_left": {
        "action": "trigger(1, 255, button(Keys.BTN_LEFT))"
    }, 
    "trigger_right": {
        "action": "trigger(254, 255, button(Keys.KEY_O))"
    }, 
    "version": 1.4
}