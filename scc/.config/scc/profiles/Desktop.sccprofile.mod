{
    "_": "", 
    "buttons": {
        "A": {
            "action": "button(Keys.KEY_ENTER)"
        }, 
        "B": {
            "action": "button(Keys.KEY_ESC)"
        }, 
        "BACK": {
            "action": "button(Keys.KEY_BACKSPACE)"
        }, 
        "C": {
            "action": "hold(menu('Default.menu'), menu('Default.menu'))"
        }, 
        "CPADPRESS": {
            "action": "button(Keys.BTN_LEFT)"
        }, 
        "LB": {
            "action": "button(Keys.BTN_RIGHT)"
        }, 
        "LGRIP": {
            "action": "button(Keys.BTN_SIDE)"
        }, 
        "RB": {
            "action": "button(Keys.KEY_LEFTALT)"
        }, 
        "RGRIP": {
            "action": "button(Keys.BTN_EXTRA)"
        }, 
        "RPAD": {
            "action": "button(Keys.BTN_LEFT)"
        }, 
        "START": {
            "action": "button(Keys.KEY_LEFTSHIFT)"
        }, 
        "X": {
            "action": "repeat(button(Keys.KEY_RIGHTCTRL))"
        }, 
        "Y": {
            "action": "button(Keys.KEY_TAB)"
        }
    }, 
    "cpad": {
        "action": "mouse()"
    }, 
    "gyro": {}, 
    "is_template": false, 
    "menus": {}, 
    "pad_left": {
        "action": "ball(XY(mouse(Rels.REL_HWHEEL), mouse(Rels.REL_WHEEL)))"
    }, 
    "pad_right": {
        "action": "sens(2.0, 2.0, ball(XY(axis(Axes.ABS_RX), axis(Axes.ABS_RY))))"
    }, 
    "stick": {
        "action": "dpad(button(Keys.KEY_LEFTMETA) and button(Keys.KEY_UP), button(Keys.KEY_LEFTMETA) and button(Keys.KEY_DOWN), button(Keys.KEY_LEFT), button(Keys.KEY_RIGHT))"
    }, 
    "trigger_left": {
        "action": "trigger(51, 255, button(Keys.BTN_LEFT))"
    }, 
    "trigger_right": {
        "action": "trigger(50, 255, button(Keys.BTN_MIDDLE))"
    }, 
    "version": 1.4
}